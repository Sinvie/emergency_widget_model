package com.sinvie.emergency_widget.pick_view.listener;

/**
 * Created by zengsong on 2018/3/21.
 */

public interface ISinvieSelectTimeCallback {

    public void onTimeSelectChanged();
}
