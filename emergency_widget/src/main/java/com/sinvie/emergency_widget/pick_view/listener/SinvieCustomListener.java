package com.sinvie.emergency_widget.pick_view.listener;

import android.view.View;

/**
 * Created by KyuYi on 2017/3/2.
 * E-Mail:kyu_yi@sina.com
 * 功能：
 */

public interface SinvieCustomListener {
    void customLayout(View v);
}
