package com.sinvie.emergency_widget.pick_view.listener;


public interface OnSinvieItemSelectedListener {
    void onItemSelected(int index);
}
