package com.sinvie.emergency_widget.pick_view.adapter;


import java.util.List;

/**
 * The simple Array wheel adapter
 * @param <T> the element type
 */
public class SinvieArrayWheelAdapter<T> implements SinvieWheelAdapter {
	

	// items
	private List<T> items;

	/**
	 * Constructor
	 * @param items the items
	 */
	public SinvieArrayWheelAdapter(List<T> items) {
		this.items = items;

	}
	
	@Override
	public Object getItem(int index) {
		if (index >= 0 && index < items.size()) {
			return items.get(index);
		}
		return "";
	}

	@Override
	public int getItemsCount() {
		return items.size();
	}

	@Override
	public int indexOf(Object o){
		return items.indexOf(o);
	}

}
