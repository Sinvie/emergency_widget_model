package com.sinvie.emergency_widget.pick_view.listener;

/**
 * Created by xiaosong on 2018/3/20.
 */

public interface OnSinvieOptionsSelectChangeListener {

    void onOptionsSelectChanged(int options1, int options2, int options3);

}
