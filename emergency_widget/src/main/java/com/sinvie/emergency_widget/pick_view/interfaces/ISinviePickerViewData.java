package com.sinvie.emergency_widget.pick_view.interfaces;

/**
 * Created by Sai on 2016/7/13.
 */
public interface ISinviePickerViewData {
    String getPickerViewText();
}
