package com.sinvie.emergency_widget.pick_view.listener;

/**
 * Created by Sai on 15/8/9.
 */
public interface OnSinvieDismissListener {
    public void onDismiss(Object o);
}
