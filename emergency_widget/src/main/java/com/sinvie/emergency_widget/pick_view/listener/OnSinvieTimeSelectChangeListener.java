package com.sinvie.emergency_widget.pick_view.listener;

import java.util.Date;

/**
 * Created by xiaosong on 2018/3/20.
 */

public interface OnSinvieTimeSelectChangeListener {

    void onTimeSelectChanged(Date date);
}
