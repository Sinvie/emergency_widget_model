package com.sinvie.emergency_widget.pick_view.listener;

import android.view.MotionEvent;

import com.sinvie.emergency_widget.pick_view.view.SinvieWheelView;


/**
 * 手势监听
 */
public final class SinvieLoopViewGestureListener extends android.view.GestureDetector.SimpleOnGestureListener {

    private final SinvieWheelView sinvieWheelView;


    public SinvieLoopViewGestureListener(SinvieWheelView sinvieWheelView) {
        this.sinvieWheelView = sinvieWheelView;
    }

    @Override
    public final boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
        sinvieWheelView.scrollBy(velocityY);
        return true;
    }
}
