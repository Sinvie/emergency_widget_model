package com.sinvie.emergency_widget.pick_view.listener;

import android.view.View;

import java.util.Date;

/**
 * Created by xiaosong on 2018/3/20.
 */

public interface OnSinvieTimeSelectListener {

    void onTimeSelect(Date date, View v);
}
