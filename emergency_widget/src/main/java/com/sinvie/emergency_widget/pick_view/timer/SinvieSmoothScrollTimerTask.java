package com.sinvie.emergency_widget.pick_view.timer;

import com.sinvie.emergency_widget.pick_view.view.SinvieWheelView;

import java.util.TimerTask;

/**
 * 平滑滚动的实现
 *
 * @author 小嵩
 */
public final class SinvieSmoothScrollTimerTask extends TimerTask {

    private int realTotalOffset;
    private int realOffset;
    private int offset;
    private final SinvieWheelView sinvieWheelView;

    public SinvieSmoothScrollTimerTask(SinvieWheelView sinvieWheelView, int offset) {
        this.sinvieWheelView = sinvieWheelView;
        this.offset = offset;
        realTotalOffset = Integer.MAX_VALUE;
        realOffset = 0;
    }

    @Override
    public final void run() {
        if (realTotalOffset == Integer.MAX_VALUE) {
            realTotalOffset = offset;
        }
        //把要滚动的范围细分成10小份，按10小份单位来重绘
        realOffset = (int) ((float) realTotalOffset * 0.1F);

        if (realOffset == 0) {
            if (realTotalOffset < 0) {
                realOffset = -1;
            } else {
                realOffset = 1;
            }
        }

        if (Math.abs(realTotalOffset) <= 1) {
            sinvieWheelView.cancelFuture();
            sinvieWheelView.getHandler().sendEmptyMessage(SinvieMessageHandler.WHAT_ITEM_SELECTED);
        } else {
            sinvieWheelView.setTotalScrollY(sinvieWheelView.getTotalScrollY() + realOffset);

            //这里如果不是循环模式，则点击空白位置需要回滚，不然就会出现选到－1 item的 情况
            if (!sinvieWheelView.isLoop()) {
                float itemHeight = sinvieWheelView.getItemHeight();
                float top = (float) (-sinvieWheelView.getInitPosition()) * itemHeight;
                float bottom = (float) (sinvieWheelView.getItemsCount() - 1 - sinvieWheelView.getInitPosition()) * itemHeight;
                if (sinvieWheelView.getTotalScrollY() <= top || sinvieWheelView.getTotalScrollY() >= bottom) {
                    sinvieWheelView.setTotalScrollY(sinvieWheelView.getTotalScrollY() - realOffset);
                    sinvieWheelView.cancelFuture();
                    sinvieWheelView.getHandler().sendEmptyMessage(SinvieMessageHandler.WHAT_ITEM_SELECTED);
                    return;
                }
            }
            sinvieWheelView.getHandler().sendEmptyMessage(SinvieMessageHandler.WHAT_INVALIDATE_LOOP_VIEW);
            realTotalOffset = realTotalOffset - realOffset;
        }
    }
}
