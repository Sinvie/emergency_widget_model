package com.sinvie.emergency_widget.pick_view.timer;

import com.sinvie.emergency_widget.pick_view.view.SinvieWheelView;

import java.util.TimerTask;

/**
 * 滚动惯性的实现
 *
 * @author 小嵩
 * date:  2017-12-23 23:20:44
 */
public final class SinvieInertiaTimerTask extends TimerTask {

    private float mCurrentVelocityY; //当前滑动速度
    private final float mFirstVelocityY;//手指离开屏幕时的初始速度
    private final SinvieWheelView mSinvieWheelView;

    /**
     * @param sinvieWheelView 滚轮对象
     * @param velocityY Y轴滑行速度
     */
    public SinvieInertiaTimerTask(SinvieWheelView sinvieWheelView, float velocityY) {
        super();
        this.mSinvieWheelView = sinvieWheelView;
        this.mFirstVelocityY = velocityY;
        mCurrentVelocityY = Integer.MAX_VALUE;
    }

    @Override
    public final void run() {

        //防止闪动，对速度做一个限制。
        if (mCurrentVelocityY == Integer.MAX_VALUE) {
            if (Math.abs(mFirstVelocityY) > 2000F) {
                mCurrentVelocityY = mFirstVelocityY > 0 ? 2000F : -2000F;
            } else {
                mCurrentVelocityY = mFirstVelocityY;
            }
        }

        //发送handler消息 处理平顺停止滚动逻辑
        if (Math.abs(mCurrentVelocityY) >= 0.0F && Math.abs(mCurrentVelocityY) <= 20F) {
            mSinvieWheelView.cancelFuture();
            mSinvieWheelView.getHandler().sendEmptyMessage(SinvieMessageHandler.WHAT_SMOOTH_SCROLL);
            return;
        }

        int dy = (int) (mCurrentVelocityY / 100F);
        mSinvieWheelView.setTotalScrollY(mSinvieWheelView.getTotalScrollY() - dy);
        if (!mSinvieWheelView.isLoop()) {
            float itemHeight = mSinvieWheelView.getItemHeight();
            float top = (-mSinvieWheelView.getInitPosition()) * itemHeight;
            float bottom = (mSinvieWheelView.getItemsCount() - 1 - mSinvieWheelView.getInitPosition()) * itemHeight;
            if (mSinvieWheelView.getTotalScrollY() - itemHeight * 0.25 < top) {
                top = mSinvieWheelView.getTotalScrollY() + dy;
            } else if (mSinvieWheelView.getTotalScrollY() + itemHeight * 0.25 > bottom) {
                bottom = mSinvieWheelView.getTotalScrollY() + dy;
            }

            if (mSinvieWheelView.getTotalScrollY() <= top) {
                mCurrentVelocityY = 40F;
                mSinvieWheelView.setTotalScrollY((int) top);
            } else if (mSinvieWheelView.getTotalScrollY() >= bottom) {
                mSinvieWheelView.setTotalScrollY((int) bottom);
                mCurrentVelocityY = -40F;
            }
        }

        if (mCurrentVelocityY < 0.0F) {
            mCurrentVelocityY = mCurrentVelocityY + 20F;
        } else {
            mCurrentVelocityY = mCurrentVelocityY - 20F;
        }

        //刷新UI
        mSinvieWheelView.getHandler().sendEmptyMessage(SinvieMessageHandler.WHAT_INVALIDATE_LOOP_VIEW);
    }
}
