package com.sinvie.emergency_widget.selector.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.LinearLayoutCompat;

import com.sinvie.emergency_widget.R;
import com.sinvie.emergency_widget.selector.SinvieDataProvider;
import com.sinvie.emergency_widget.selector.ISinvieSelectAble;
import com.sinvie.emergency_widget.selector.SinvieSelectAdapter;
import com.sinvie.emergency_widget.selector.SinvieSelectedListener;

import java.util.ArrayList;
import java.util.List;

/**
 * @author: JonsonBob
 * @date: 2021/7/28
 * @Des: 类描述
 */
public class SinvieSelectorLayout extends LinearLayoutCompat implements AdapterView.OnItemClickListener {

    public final int INDEX_INVALID = -1;
    private Context mContext;
    private SinvieSelectedListener listener;
    private View mIndicator;
    private LinearLayout mLinearLayout;
    private ProgressBar mProgressBar;

    private ListView mListView;


    private int mTabIndex = 0;

    //所有Adapter数据
    private List<List<ISinvieSelectAble>> mAllDatas = new ArrayList<>();
    //每个ListView Adapter
    private List<SinvieSelectAdapter> mSinvieSelectAdapterList = new ArrayList<>();
    //记录每个列表选中下标
    private ArrayList<Integer> mIntegerArrayList = new ArrayList<>();
    // 数据接口
    SinvieDataProvider mSinvieDataProvider;
    //标题栏
    private List<TextView> mTabsTitle = new ArrayList<>();

    /**
     * 点击ListView Item  更新选中状态
     */
    private int mAllTableIndex;//总展开层级数

    /**
     * 填充数据
     *
     * @param sinvieDataProvider 数据类
     */
    public void setDataProvider(SinvieDataProvider sinvieDataProvider) {
        this.mSinvieDataProvider = sinvieDataProvider;
        getNextData(-1, null);
    }

    public void setDefaultSelector(int position) {
        if (mListView != null) {
            mListView.performItemClick(mListView.getChildAt(position), position, mListView.getItemIdAtPosition(position));
        }
    }

    public SinvieSelectorLayout(@NonNull Context context) {
        super(context);
        mContext = context;
        initView();
    }

    public SinvieSelectorLayout(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        mContext = context;
        initView();
    }

    public SinvieSelectorLayout(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mContext = context;
        initView();
    }

    private void initView() {
        inflate(mContext, R.layout.view_sinvie_address_selector, this);
        mProgressBar = findViewById(R.id.progressBar);
        mListView = findViewById(R.id.listView);
        mIndicator = findViewById(R.id.indicator);
        mLinearLayout = findViewById(R.id.layout_tab);

        mListView.setOnItemClickListener(this);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        mIntegerArrayList.set(mTabIndex, position);
        ISinvieSelectAble selectAble = mAllDatas.get(mTabIndex).get(position);
        // 更新当前级别及子级标签文本
        mTabsTitle.get(mTabIndex).setText(selectAble.getName());
        //清空数据集
        if (mTabIndex + 1 < mAllDatas.size()) {

            for (int i = mTabIndex + 1; i < mTabsTitle.size(); i++) {
                mLinearLayout.removeView(mTabsTitle.get(i));
            }

            mAllDatas.subList(mTabIndex + 1, mAllDatas.size()).clear();
            mSinvieSelectAdapterList.subList(mTabIndex + 1, mSinvieSelectAdapterList.size()).clear();
            mTabsTitle.subList(mTabIndex + 1, mTabsTitle.size()).clear();
            mIntegerArrayList.subList(mTabIndex + 1, mIntegerArrayList.size()).clear();
            mSinvieSelectAdapterList.get(mTabIndex).notifyDataSetChanged();

        }
        // 更新已选中项
        this.mSinvieSelectAdapterList.get(mTabIndex).setSelectedIndex(position);
        this.mSinvieSelectAdapterList.get(mTabIndex).notifyDataSetChanged();

        updateTabsVisibility(mTabIndex);
        //查询下级数据
        getNextData(selectAble.getId(), selectAble);
    }

    /**
     * 根据当前集合选择的id，向用户获取下一级子集的数据
     *
     * @param preId
     * @param selectAble
     */
    private void getNextData(final int preId, ISinvieSelectAble selectAble) {
        if (mSinvieDataProvider == null) {
            return;
        }
        mProgressBar.setVisibility(View.VISIBLE);
        mSinvieDataProvider.provideData(mTabIndex, preId, selectAble, new SinvieDataProvider.DataReceiver() {
            @Override
            public void send(List<ISinvieSelectAble> data) {

                // 有数据返回
                if (data != null && data.size() != 0) {
                    if (preId != -1) {
                        mTabIndex = mTabIndex + 1;
                    }
                    //判断展开层级 增加
                    if (mTabIndex <= mAllDatas.size()) {
                        mAllDatas.add(data);
                        mAllTableIndex = mAllDatas.size();
                        mSinvieSelectAdapterList.add(new SinvieSelectAdapter(mAllDatas.get(mTabIndex)));
                        mSinvieSelectAdapterList.get(mTabIndex).notifyDataSetChanged();
                        mListView.setAdapter(mSinvieSelectAdapterList.get(mTabIndex));
                        TextView textView = (TextView) LayoutInflater.from(mContext).inflate(R.layout.view_sinvie_simple_text, mLinearLayout, false);
                        textView.setTag(mTabIndex);
                        textView.setOnClickListener(new TextViewClickListener());
                        mLinearLayout.addView(textView);
                        mTabsTitle.add(textView);
                        mIntegerArrayList.add(-1);

                    }

                } else {
                    callbackInternal();
                }

                updateTabsVisibility(mTabIndex);
                updateProgressVisibility();

            }
        });
    }

    private void callbackInternal() {
        if (listener != null) {
            ArrayList<ISinvieSelectAble> result = new ArrayList<>(mAllDatas.size());
            for (int i = 0; i < mAllDatas.size(); i++) {
                ISinvieSelectAble resultBean = mAllDatas.get(i) == null
                        || mIntegerArrayList.get(i) == INDEX_INVALID ? null
                        : mAllDatas.get(i).get(mIntegerArrayList.get(i));
                result.add(resultBean);
            }
            listener.onAddressSelected(result);
        }
    }

    /**
     * 控制 tab的显示隐藏
     *
     * @param index
     */
    private void updateTabsVisibility(int index) {
        for (int i = 0; i < mTabsTitle.size(); i++) {
            TextView tv = mTabsTitle.get(i);
            tv.setVisibility(mAllDatas.get(i).size() != 0 ? View.VISIBLE : View.GONE);
//            tv.setVisibility(View.VISIBLE);
            tv.setEnabled(index != i);
        }
    }

    private void updateProgressVisibility() {
        ListAdapter adapter = mListView.getAdapter();
        int itemCount = adapter.getCount();
        mProgressBar.setVisibility(itemCount > 0 ? View.GONE : View.VISIBLE);
    }

    class TextViewClickListener implements OnClickListener {

        @Override
        public void onClick(View v) {
            //设置tab 下标
            mTabIndex = (int) v.getTag();
            //更新adapter
            mListView.setAdapter(mSinvieSelectAdapterList.get(mTabIndex));
            //设置选择位置
            if (mIntegerArrayList.get(mTabIndex) != INDEX_INVALID) {
                mListView.setSelection(mIntegerArrayList.get(mTabIndex));
            }
            updateTabsVisibility(mTabIndex);
        }
    }

    public SinvieSelectedListener getOnAddressSelectedListener() {
        return listener;
    }

    public void setSelectedListener(SinvieSelectedListener listener) {
        this.listener = listener;
    }

}
