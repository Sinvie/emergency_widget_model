package com.sinvie.emergency_widget.selector;

import android.app.Dialog;
import android.content.Context;
import android.view.Gravity;
import android.view.Window;
import android.view.WindowManager;

import com.sinvie.emergency_widget.R;


public class SinvieBottomDialog extends Dialog {
    private SinvieSelector selector;

    public SinvieBottomDialog(Context context) {
        super(context, R.style.sinvie_bottom_dialog);
    }

    public SinvieBottomDialog(Context context, int themeResId) {
        super(context, themeResId);
    }

    public SinvieBottomDialog(Context context, boolean cancelable, OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
    }

    public void init(Context context, SinvieSelector selector) {
        this.selector = selector;
        setContentView(selector.getView());
        Window window = getWindow();
        WindowManager.LayoutParams params = window.getAttributes();
        params.width = WindowManager.LayoutParams.MATCH_PARENT;
        params.height = dip2px(context, 456);
        window.setAttributes(params);

        window.setGravity(Gravity.BOTTOM);
    }

    public static int dip2px(Context context, float dpValue) {
        final float scale = context.getResources().getDisplayMetrics().density;
        return (int) (dpValue * scale + 0.5f);
    }
    public void setOnAddressSelectedListener(SinvieSelectedListener listener) {
        this.selector.setSelectedListener(listener);
    }

    public static SinvieBottomDialog show(Context context) {
        return show(context, null);
    }

    public static SinvieBottomDialog show(Context context, SinvieSelectedListener listener) {
        SinvieBottomDialog dialog = new SinvieBottomDialog(context, R.style.sinvie_bottom_dialog);
        dialog.selector.setSelectedListener(listener);
        dialog.show();

        return dialog;
    }
}
