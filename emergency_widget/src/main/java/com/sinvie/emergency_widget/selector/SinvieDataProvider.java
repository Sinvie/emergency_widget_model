package com.sinvie.emergency_widget.selector;

import java.util.List;

/**
 * Created by dun on 17/2/9.
 */

public interface SinvieDataProvider {
    void provideData(int currentDeep, int preId, ISinvieSelectAble selectAble, DataReceiver receiver);


    interface DataReceiver {
        void send(List<ISinvieSelectAble> data);
    }
}
