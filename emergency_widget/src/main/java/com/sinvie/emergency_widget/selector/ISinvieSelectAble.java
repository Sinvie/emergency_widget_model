package com.sinvie.emergency_widget.selector;

/**
 * Created by dun on 17/2/9.
 */

public interface ISinvieSelectAble {
    /**
     * 显示在栏目上的名字
     * */
    String getName();

    /**
     * 用户设定的id，根据这个id，可以获取级栏目或者指定为最终栏目的id
     * */
    int getId();

    /**
     * 自定义类型对象。
     * */
    Object getArg();

}
