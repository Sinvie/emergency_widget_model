package com.sinvie.emergency_widget.selector.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.ListView;

public class SinvieUninterceptableListView extends ListView {
    public SinvieUninterceptableListView(Context context) {
        super(context);
    }

    public SinvieUninterceptableListView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public SinvieUninterceptableListView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public boolean onTouchEvent(MotionEvent ev) {
        getParent().requestDisallowInterceptTouchEvent(true);
        return super.onTouchEvent(ev);
    }
}
